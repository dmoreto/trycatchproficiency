require 'rails_helper'

RSpec.describe Api::V1::FollowsController, type: :controller do
  describe "POST #follow" do
    let(:followed_user) { create :user, email: "followed@teste.com" }

    context "with an unauthorized user" do
      it "can't follow another user" do
        post :follow, user_id: followed_user.id
        expect(json_response[:errors]).to be_present
      end
    end

    context "with an authorized user" do
      send_auth_user

      context "when are not following yet" do
        it "include user in followings list" do
          post :follow, user_id: followed_user.id
          expect(@user.my_followings).to include followed_user
        end

        it "retuns status 201" do
          post :follow, user_id: followed_user.id
          expect(response.status).to eq 201
        end

        it "returns following count of followed user" do
          post :follow, user_id: followed_user.id
          expect(json_response[:followers]).to be_present
        end
      end

      context "when already be following" do

        before { create(:follow, follower: @user, followed: followed_user) }

        it "return status 402" do
          post :follow, user_id: followed_user.id
          expect(response.status).to eq 402
        end
      end
    end
  end

  describe "DELETE #unfollow" do
    let(:followed_user) { create :user, email: "followed@teste.com" }

    context "with an unauthorized user" do
      it "can't unfollow another user" do
        post :unfollow, user_id: followed_user.id
        expect(json_response[:errors]).to be_present
      end
    end

    context "with an authorized user" do
      send_auth_user
      before(:each) { create(:follow, follower: @user, followed: followed_user) }

      it "stops to follow an user" do
        delete :unfollow, user_id: followed_user.id
        expect(@user.my_followings).to_not include followed_user
      end

      it "returns status 201" do
        delete :unfollow, user_id: followed_user.id
        expect(response.status).to eq 201
      end

      it "returns followers count of unfollowed user" do
        delete :unfollow, user_id: followed_user.id
        expect(json_response[:followers]).to be_present
      end
    end
  end

  describe "GET #followings" do
    let!(:other_user) { create :with_many_followeds }

    it "assigns @followings" do
      get :followings, user_id: other_user.id
      expect(assigns(:followings)).to eq other_user.my_followings
    end

    it "returns followings json" do
      get :followings, user_id: other_user.id
      expect(json_response[:followings]).to be_present
    end

    it "return status 200" do
      get :followings, user_id: other_user.id
      expect(response.status).to eq 200
    end
  end

  describe "GET #followers" do
    let!(:other_user) { create :with_many_followers }

    it "assigns @followers" do
      get :followers, user_id: other_user.id
      expect(assigns(:followers)).to eq other_user.my_followers
    end

    it "returns followers json" do
      get :followers, user_id: other_user.id
      expect(json_response[:followers]).to be_present
    end

    it "return status 200" do
      get :followers, user_id: other_user.id
      expect(response.status).to eq 200
    end
  end
end
