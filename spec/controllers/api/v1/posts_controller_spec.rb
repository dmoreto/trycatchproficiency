require 'rails_helper'

RSpec.describe Api::V1::PostsController, type: :controller do
  describe "GET #index" do
    let(:user_post) { create(:with_posts) }

    it "returns all users @posts" do
      get :index, user_id: user_post.id
      expect(assigns(:posts)).to eq user_post.posts
    end

    it "returns status 200" do
      get :index, user_id: user_post.id
      expect(response.status).to eq 200
    end
  end

  describe "POST #create" do
    context "when is not current-user" do
      let(:user) { create(:user) }

      it "don't create with other admin user" do
        api_authorization_header create(:admin, email: "env_teste@teste.com")
        expect {
          post :create, user_id: user.id, post: attributes_for(:post)
        }.to_not change(Post, :count)
      end

      it "don't create with other non-admin user" do
        api_authorization_header create(:user, email: "env_teste@teste.com")
        expect {
          post :create, user_id: user.id, post: attributes_for(:post)
        }.to_not change(Post, :count)
      end
    end

    context "when is current-user" do
      before do
        @user = create(:user, email: "env_teste@teste.com")
        api_authorization_header @user.auth_token
      end

      context "with valid attributes" do
        let(:valid_post) { attributes_for :post }

        it "create new Post" do
          expect {
            post :create, user_id: @user.id, post: valid_post
          }.to change(Post, :count).by(1)
        end

        it "assigns created @post" do
          post :create, user_id: @user.id, post: valid_post
          expect(assigns(:post)).to be_present
        end

        it "returns status 201" do
          post :create, user_id: @user.id, post: valid_post
          expect(response.status).to eq 201
        end
      end

      context "with invalid attributes" do
        let(:invalid_post) { attributes_for(:post, content: '') }

        it "doesn't create a new Post" do
          expect {
            post :create, user_id: @user.id, post: invalid_post
          }.to_not change(Post, :count)
        end

        it "return @post.errors" do
          post :create, user_id: @user.id, post: invalid_post
          expect(json_response[:errors]).to be_present
        end

        it "return status 422" do
          post :create, user_id: @user.id, post: invalid_post
          expect(response.status).to eq 422
        end
      end
    end
  end

  describe "GET #show" do
    let(:user) { create :user }
    let(:post) { create :post, user: user }

    it "returns @post info" do
      get :show, user_id: user.id, id: post.id
      expect(json_response[:post]).to be_present
    end

    it "assigns @post" do
      get :show, user_id: user.id, id: post.id
      expect(assigns(:post)).to eq post
    end

    it "return status 200" do
      get :show, user_id: user.id, id: post.id
      expect(response.status).to eq 200
    end
  end

  describe "PATCH #update" do
    context "when is not current-user" do
      let(:user) { create(:user) }
      let!(:post) { create :post, user: user }

      it "don't update with other admin user" do
        api_authorization_header create(:admin, email: "env_teste@teste.com")
        patch :update, user_id: user.id, id: post.id, post: attributes_for(:post, content: "Changing post content")
        expect(post[:content]).to_not eq 'Changing post content'
      end

      it "don't update with other non-admin user" do
        api_authorization_header create(:user, email: "env_teste@teste.com")
        patch :update, user_id: user.id, id: post.id, post: attributes_for(:post, content: "Changing post content")
        expect(post[:content]).to_not eq 'Changing post content'
      end
    end

    context "when is current-user" do
      before do
        @user = create(:user, email: "env_teste@teste.com")
        api_authorization_header @user.auth_token
      end

      let!(:post) { create :post, user: @user }

      context "with valid attributes" do
        it "updates @post" do
          patch :update, user_id: @user.id, id: post.id, post: attributes_for(:post, content: "Changing post content")
          post.reload
          expect(post[:content]).to eq 'Changing post content'
        end

        it "assigns updated @post" do
          patch :update, user_id: @user.id, id: post.id, post: attributes_for(:post, content: "Changing post content")
          expect(assigns(:post)).to be_present
        end

        it "returns status 201" do
          patch :update, user_id: @user.id, id: post.id, post: attributes_for(:post, content: "Changing post content")
          expect(response.status).to eq 201
        end
      end

      context "with invalid attributes" do
        it "return @post.errors" do
          patch :update, user_id: @user.id, id: post.id, post: attributes_for(:post, content: "")
          expect(json_response[:errors]).to be_present
        end

        it "return status 422" do
          patch :update, user_id: @user.id, id: post.id, post: attributes_for(:post, content: "")
          expect(response.status).to eq 422
        end
      end
    end
  end

  describe "DELETE #destroy" do
    let!(:user) { create :user }

    context "when is non-admin user" do
      before do
        @user = create(:user, email: 'other_user@test.com')
        api_authorization_header @user.auth_token
      end

      it "returns status 204 when try do destroy its own post" do
        post = create :post, user: @user
        delete :destroy, user_id: @user.id, id: post.id
        expect(response.status).to eq 204
      end

      it "returns status 422 if try to destroy other user's post" do
        post = create :post, user: user
        delete :destroy, user_id: user.id, id: post.id
        expect(response.status).to eq 422
      end
    end

    context "when is admin user" do
      before do
        @user = create(:admin, email: 'other_user@test.com')
        api_authorization_header @user.auth_token
      end

      it "returns status 204 when try do destroy its own post" do
        post = create :post, user: @user
        delete :destroy, user_id: @user.id, id: post.id
        expect(response.status).to eq 204
      end

      it "returns status 204 if try to destroy other user's post" do
        post = create :post, user: user
        delete :destroy, user_id: user.id, id: post.id
        expect(response.status).to eq 204
      end
    end
  end
end
