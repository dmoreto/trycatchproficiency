require 'rails_helper'

RSpec.describe Api::V1::SessionsController, type: :controller do
  before do
    @user = create(:user, email: "env_teste@teste.com")
    api_authorization_header @user.auth_token
  end

  describe "POST #create" do
    let!(:user) { create(:user) }

    context "when credentials are correct" do
      let!(:credentials) { { email: user.email, password: user.password } }

      it "returns the user auth_token corresponding to the credentials" do
        post :create, session: credentials
        user.reload
        expect(json_response[:auth]).to eq user.auth_token
      end

      it "responds with status 200" do
        post :create, session: credentials
        expect(response.status).to eq 200
      end
    end

    context "when credentials are incorrect" do
      let!(:credentials) { { email: user.email, password: 'idontknowmypass'} }

      it "retuns a json with error" do
        post :create, session: credentials
        expect(json_response[:errors]).to eq "Invalid email or password"
      end

      it "responds with status 422" do
        post :create, session: credentials
        expect(response.status).to eq 422
      end
    end
  end

  describe "DELETE #destroy" do
    let!(:user) { create :user }

    it "responds with status 204" do
      sign_in user
      delete :destroy, id: user.auth_token
      expect(response.status).to eq 204
    end
  end
end
