require 'rails_helper'

RSpec.describe Api::V1::UsersController, type: :controller do
  describe "GET #index" do
    let!(:users) { create_list :user, 3 }

    it "returns all @users" do
      get :index
      expect(assigns(:users)).to eq User.all
    end

    it "returns status 200" do
      get :index
      expect(response.status).to eq 200
    end
  end

  describe "POST #create" do
    it "fails if try to create as non-admin" do
      api_authorization_header create(:user).auth_token
      expect {
        post :create, user: attributes_for(:user)
      }.to_not change(User, :count)
    end

    context "with valid attributes" do
      send_auth_admin

      let(:valid_user) { attributes_for :user }

      it "create a new user" do
        expect {
          post :create, user: valid_user
        }.to change(User, :count).by(1)
      end

      it "assigns created @user" do
        post :create, user: valid_user
        expect(assigns(:user)).to be_present
      end

      it "returns status 201" do
        post :create, user: valid_user
        expect(response.status).to eq 201
      end
    end

    context "with invalid attributes" do
      send_auth_admin

      let(:invalid_user) { attributes_for(:user, email: '') }

      it "doesn't create a new User" do
        expect {
          post :create, user: invalid_user
        }.to_not change(User, :count)
      end

      it "return @user.errors" do
        post :create, user: invalid_user
        expect(json_response[:errors]).to be_present
      end

      it "return status 422" do
        post :create, user: invalid_user
        expect(response.status).to eq 422
      end
    end
  end

  describe "GET #show" do
    let!(:user) { create :user }

    it "returns @user info" do
      get :show, id: user.id
      expect(json_response[:email]).to be_present
    end

    it "assigns @user" do
      get :show, id: user.id
      expect(assigns(:user)).to eq user
    end

    it "return status 200" do
      get :show, id: user.id
      expect(response.status).to eq 200
    end
  end

  describe "PATCH #update" do
    let!(:user) { create :user }

    context "when is non-admin user" do
      before do
        @user = create(:user, email: 'other_user@test.com')
        api_authorization_header @user.auth_token
      end

      it "fails if try to update as non-current-user" do
        post :update, id: user.id, user: attributes_for(:user, email: 'test@domain.com')
        user.reload
        expect(user[:email]).to_not eq 'test@domain.com'
      end

      it "pass if try to update current-user" do
        post :update, id: @user.id, user: attributes_for(:user, email: 'test@domain.com')
        @user.reload
        expect(@user[:email]).to eq 'test@domain.com'
      end
    end

    context "when is admin" do
      send_auth_admin

      context "with valid attributes" do
        it "updates @user" do
          post :update, id: user.id, user: attributes_for(:user, email: 'teste@domain.com')
          user.reload
          expect(user[:email]).to eq 'teste@domain.com'
        end

        it "assigns updated @user" do
          post :update, id: user.id, user: attributes_for(:user, email: 'teste@domain.com')
          expect(assigns(:user)).to be_present
        end

        it "returns status 201" do
          post :update, id: user.id, user: attributes_for(:user, email: 'teste@domain.com')
          expect(response.status).to eq 201
        end
      end

      context "with invalid attributes" do
        it "return @user.errors" do
          post :update, id: user.id, user: attributes_for(:user, email: '')
          expect(json_response[:errors]).to be_present
        end

        it "return status 422" do
          post :update, id: user.id, user: attributes_for(:user, email: '')
          expect(response.status).to eq 422
        end
      end
    end
  end

  describe "DELETE #destroy" do
    let!(:user) { create :user }

    context "when is non-admin user" do
      before do
        @user = create(:user, email: 'other_user@test.com')
        api_authorization_header @user.auth_token
      end

      it "returns status 204 if current-user try to destroy itself" do
        delete :destroy, id: @user.id
        expect(response.status).to eq 204
      end

      it "returns status 422 if current-user try to destroy other user" do
        delete :destroy, id: user.id
        expect(response.status).to eq 422
      end
    end

    context "when is admin user" do
      before do
        @user = create(:admin, email: 'other_user@test.com')
        api_authorization_header @user.auth_token
      end

      it "returns 204 if try to destroy itself" do
        delete :destroy, id: @user.id
        expect(response.status).to eq 204
      end

      it "returns 204 if try to destroy other user" do
        delete :destroy, id: user.id
        expect(response.status).to eq 204
      end
    end
  end
end
