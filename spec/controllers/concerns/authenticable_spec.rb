require "rails_helper"

class Authentication < ActionController::Base
  include Authenticable
end

describe Authenticable do
  let(:authentication) { Authentication.new }
  subject { authentication }

  describe "#current_user" do
    before do
      @user = create :user
      request.headers["Authorization"] = @user.auth_token
      authentication.stub(:request).and_return(request)
    end

    it "returns the user from the authorization header" do
      expect(authentication.current_user.auth_token).to eq @user.auth_token
    end
  end

  describe "#verify_user_role" do
    let(:guest) { create :guest }
    before do
      @current_user = User.new
      authentication.stub(:current_user).and_return(@current_user)
    end

    it "add :guest role to user" do
      authentication.verify_user_role!
      expect(@current_user.has_role?(:guest)).to be_truthy
    end
  end

  describe "#user_authorized?" do
    before do
      @user = create(:user)
      request.headers['Authorization'] = @user.auth_token
      allow(authentication).to receive(:request).and_return(request)
    end

    it "returns true when the header 'Authorization' is present" do
      expect(authentication.user_authorized?).to be_truthy
    end

    it "assigns @current_user" do
      authentication.user_authorized?
      expect(authentication.current_user).to eq @user
    end
  end
end
