FactoryGirl.define do
  factory :user do
    email { Faker::Internet.email }
    name { Faker::Name.name }
    password '12345678'
    password_confirmation '12345678'
    after(:create) { |user| user.add_role(:user) }

    factory :admin do
      after(:create) { |user| user.add_role(:admin) }
    end

    factory :guest do
      after(:create) do |user|
        user.remove_role(:admin)
        user.remove_role(:user)
      end
    end

    factory :with_many_followeds do
      # If the user has many followeds, he is the follower of this records
      after(:create) { |user| create_list(:follow, 10, follower: user) }
    end

    factory :with_many_followers do
      # If the user has many followers, he is the followed of this records
      after(:create) { |user| create_list(:follow, 10, followed: user) }
    end

    factory :with_posts do
      after(:create) { |user| create_list :post, 10, user: user }
    end

  end
end
