require 'rails_helper'

RSpec.describe Follow, type: :model do
  it { is_expected.to belong_to :follower }
  it { is_expected.to belong_to :followed }
  it { is_expected.to validate_uniqueness_of(:follower_id).scoped_to(:followed_id) }
end
