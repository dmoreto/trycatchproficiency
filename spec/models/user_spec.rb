require 'rails_helper'

RSpec.describe User, type: :model do

  before { @user = build :user }

  it { is_expected.to validate_presence_of :email }
  it { is_expected.to validate_uniqueness_of(:email).case_insensitive }
  it { is_expected.to validate_presence_of :password }
  it { is_expected.to validate_confirmation_of :password }
  it { is_expected.to validate_uniqueness_of(:auth_token) }
  it { is_expected.to have_many :followers }
  it { is_expected.to have_many :followeds }
  it { is_expected.to have_many :my_followings }
  it { is_expected.to have_many :my_followers }

  it "#generate_authentication_token!" do
    Devise.stub(:friendly_token).and_return("thisisanuniquetoken")
    @user.generate_authentication_token!
    expect(@user.auth_token).to eq "thisisanuniquetoken"
  end

  it "generates other token when one already been taken" do
    first_user = create(:user, auth_token: "thisisanuniquetoken")
    @user.generate_authentication_token!
    expect(@user.auth_token).to_not eq first_user.auth_token
  end

end
