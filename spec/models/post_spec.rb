require 'rails_helper'

RSpec.describe Post, type: :model do
  it { is_expected.to validate_presence_of(:content)  }
  it { is_expected.to validate_length_of(:content).is_at_least(1).is_at_most(500) }
  it { is_expected.to belong_to :user }
end
