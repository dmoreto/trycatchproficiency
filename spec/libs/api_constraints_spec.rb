require 'rails_helper'

describe ApiConstraints do
  let(:api_constraint_v1) { ApiConstraints.new(version: 1) }
  let(:api_constraint_v2) { ApiConstraints.new(version: 2, default: true) }

  describe "matches?" do
    it "returns true when the version matches with the 'Accept' header" do
      request = double(host: 'api.trycatchproficiency.dev',
                       headers: {"Accept" => "application/vnd.trycatchproficiency.v1"})
      expect(api_constraint_v1.matches?(request)).to be_truthy
    end

    it "returns true when the version matches with the default version" do
      request = double(host: 'api.trycatchproficiency.dev')
      expect(api_constraint_v2.matches?(request)).to be_truthy
    end
  end
end
