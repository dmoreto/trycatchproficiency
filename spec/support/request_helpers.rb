module Request
  module JsonHelpers
    def json_response
      @json_response ||= JSON.parse(response.body, symbolize_names: true)
    end
  end

  module HeadersHelpers
    def api_header(version = 1)
      request.headers['Accept'] = "application/vnd.marketplace.v#{version}"
    end

    def api_response_format(format = Mime::JSON)
      request.headers['Accept'] = "#{request.headers['Accept']},#{format}"
      request.headers['Content-Type'] = format.to_s
    end

    def api_authorization_header(token)
      request.headers['Authorization'] =  token
    end

    def include_default_accept_headers
      api_header
      api_response_format
    end
  end

  module AuthorizationHelpers
    def send_auth_user
      before do
        @user = create(:user, email: "env_test@test.com")
        api_authorization_header @user.auth_token
      end
    end

    def send_auth_admin
      before do
        @user = create(:admin, email: "env_test@test.com")
        api_authorization_header @user.auth_token
      end
    end
  end
end


RSpec.configure do |config|
  config.include Request::JsonHelpers, :type => :controller
  config.include Request::HeadersHelpers, :type => :controller
  config.extend Request::AuthorizationHelpers, :type => :controller

  config.before(:each, type: :controller) do
    include_default_accept_headers
  end
end
