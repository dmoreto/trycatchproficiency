class CreateFollows < ActiveRecord::Migration
  def change
    create_table :follows do |t|
      t.timestamps null: false
    end

    add_reference :follows, :follower, references: :users, index: true
    add_reference :follows, :followed, references: :users, index: true
  end
end
