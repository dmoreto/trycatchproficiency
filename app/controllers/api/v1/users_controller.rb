class Api::V1::UsersController < ApiController

  before_filter :authenticate_user!, only: [:create, :update, :destroy]

  def index
    @users = if user_authorized?
                User.where("id != #{current_user.id}")
             else
                User.all
             end
    render json: @users
  end

  def create
    authorize User
    @user = User.new(user_params)

    if @user.save
      render json: @user, status: 201, location: [:api, @user]
    else
      render json: { errors: @user.errors }, status: 422
    end
  end

  def show
    @user = User.find(params[:id])
    render json: @user
  end

  def update
    @user = User.find(params[:id])
    authorize @user, :admin_or_current_user?

    if @user.update(user_params)
      render json: @user, status: 201, location: [:api, @user]
    else
      render json: { errors: @user.errors }, status: 422
    end
  end

  def destroy
    @user = User.find(params[:id])
    authorize @user, :admin_or_current_user?
    @user.destroy
    head 204
  end


  private

  def user_params
    params.require(:user).permit(:email, :password, :password_confirmation)
  end

end
