class Api::V1::FollowsController < ApiController

  before_action :stops_unauthorized_user!, only: [:follow, :unfollow]

  def follow
    @follow = Follow.new(follower_id: current_user.id, followed_id: params[:user_id])
    if @follow.save
      render json: { followers: @follow.followed.my_followers.count }, status: 201
    else
      head 402
    end
  end

  def unfollow
    @follow = Follow.find_by(follower_id: current_user.id, followed_id: params[:user_id]).destroy
    render json: { followers: @follow.followed.my_followers.count }, status: 201
  end

  def followings
    @followings = User.find(params[:user_id]).my_followings.select(:id, :name, :email)
    render json: { followings: @followings }, status: 200
  end

  def followers
    @followers = User.find(params[:user_id]).my_followers.select(:id, :name, :email)
    render json: { followers: @followers }, status: 200
  end

  private

  def stops_unauthorized_user!
    render json: { errors: "Not authorized" }, status: 422 unless user_authorized?
  end
end
