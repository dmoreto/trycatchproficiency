class Api::V1::PostsController < ApiController

  before_filter :authenticate_user!, except: [:index]

  def index
    @posts = Post.where(user_id: params[:user_id])
    render json: @posts, status: 200
  end

  def create
    authorize User.find(params[:user_id]), :is_current_user?
    @post = Post.new(post_params)
    if @post.save
      render json: @post, status: 201
    else
      render json: { errors: @post.errors }, status: 422
    end
  end

  def show
    @post = Post.find(params[:id])
    render json: { post: @post }, status: 200
  end

  def update
    @post = Post.find(params[:id])
    if @post.update(post_params)
      render json: @post, status: 201
    else
      render json: { errors: @post.errors }, status: 422
    end
  end

  def destroy
    authorize User.find(params[:user_id]), :admin_or_current_user?
    Post.find(params[:id]).destroy
    head 204
  end

  private

  def post_params
    params.require(:post).permit(:user_id, :content)
  end
end
