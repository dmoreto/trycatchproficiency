class DashboardController < ApplicationController
  def index
    render 'devise/sessions/new', resource: User.new unless user_signed_in?
  end
end
