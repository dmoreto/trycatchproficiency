module Authenticable
  def current_user
    @current_user ||= User.find_by(auth_token: request.headers['Authorization'])
    @current_user ||= User.new
  end

  def verify_user_role!
    current_user.add_role(:guest) unless current_user.has_role?(:admin) || current_user.has_role?(:user)
  end

  def user_authorized?
    return true if @current_user = User.find_by(auth_token: request.headers['Authorization'])
    return false
  end
end
