class ApiController < ActionController::Base
  include Authenticable

  protect_from_forgery with: :null_session
  before_action :verify_user_role!

  include Pundit

  rescue_from Pundit::NotAuthorizedError, with: :user_not_authorized

  private

  def user_not_authorized
    render json: { errors: "Not authorized" }, status: 422
  end
end
