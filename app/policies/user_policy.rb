class UserPolicy < ApplicationPolicy
  def create?
    user.has_role?(:admin)
  end

  def admin_or_current_user?
    user.has_role?(:admin) || user.id == record.id
  end

  def is_current_user?
    user.id == record.id
  end
end
