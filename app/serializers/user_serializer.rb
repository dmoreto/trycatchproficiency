class UserSerializer < ActiveModel::Serializer
  attributes :id, :name, :email, :followers, :is_followed, :auth_token, :is_admin

  def followers
    object.my_followers.length
  end

  def is_followed
    object.my_followers.include?(current_user)
  end

  def auth_token
    return object.auth_token if object.id == current_user.id
  end

  def is_admin
    return object.has_role? :admin
  end

end
