angular.module('trycatch').controller("UsersController", ['$scope', '$http', 'Auth', 'TokenGen', function($scope, $http, Auth, TokenGen){

  $scope.users = [];

  Auth.currentUser().then(function(current_user){
    list_users();
  }, function(){
    list_users();
  });

  list_users = function(){
    $http.get("/api/users", { headers: { "Authorization": TokenGen.getToken() } })
         .success(function(users){
           $scope.users = users;
         })
         .error(function(){
           alert("An error ocurred while trying to get users");
         });
  };

  $scope.follow = function(user){
    $http.post("/api/users/" +user.id+ "/follow", {}, { headers: { "Authorization": TokenGen.getToken() } })
        .success(function(data){
          $scope.users[$scope.users.indexOf(user)].followers = data.followers;
          $scope.users[$scope.users.indexOf(user)].is_followed = true;
        })
        .error(function(){
          alert("An error ocurred while trying to follow user");
        });
  };

  $scope.unfollow = function(user){
    $http.delete("/api/users/" +user.id+ "/unfollow", { headers: { "Authorization": TokenGen.getToken() } })
        .success(function(data){
          $scope.users[$scope.users.indexOf(user)].followers = data.followers;
          $scope.users[$scope.users.indexOf(user)].is_followed = false;
        })
        .error(function(){
          alert("An error ocurred while trying to unfollow user");
        });
  };

  $scope.delete = function(user){
    $http.delete("/api/users/" +user.id, { headers: { "Authorization": TokenGen.getToken() } })
        .success(function(data){
          $scope.users.splice($scope.users.indexOf(user), 1);
        })
        .error(function(){
          alert("An error ocurred while trying to delete user");
        });
  };

}]);
