// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require angular
//= require angular-bootstrap
//= require angular-ui-router
//= require angular-rails-templates
//= require turbolinks
//= require angular-devise
//= require_tree ../templates
//= require application
//= require_tree .

angular.module('trycatch',['ui.router', 'templates', 'Devise']);

angular.module('trycatch').config(['$stateProvider', '$httpProvider', function($stateProvider, $httpProvider) {

  $httpProvider.defaults.headers.common["Accept"] = "application/json,vnd.trycatchproficiency.v1";
  $httpProvider.defaults.headers.common["X-CSRF-Token"] = $("meta[name='csrf-token']").prop("content");

  $stateProvider.state('api', {
    url: "",
    template: "<ui-view/>",
    abstract: true
  });

  $stateProvider.state('api.users', {
      url: '/',
      templateUrl: 'users/users_list.html',
      controller: 'UsersController'
  });

  $stateProvider.state('api.follows', {
      url: '/follows',
      templateUrl: 'follows_list.html',
      controller: "FollowsController"
  });

  $stateProvider.state('api.posts', {
    abstract: true,
    template: "<ui-view/>",
    url: '/posts/:user_id'
  });

  $stateProvider.state('api.posts.add', {
    url: '/add',
    templateUrl: 'posts/post_add.html',
    controller: "PostAddController"
  });

  $stateProvider.state('api.posts.list', {
    url: '/list',
    templateUrl: 'posts/posts_list.html',
    controller: "PostsController"
  });

  $stateProvider.state('api.posts.edit', {
    url: '/edit/:post_id',
    templateUrl: 'posts/post_edit.html',
    controller: "PostEditController"
  });

}]).run(function($state){
  $state.go("api.users");
});
