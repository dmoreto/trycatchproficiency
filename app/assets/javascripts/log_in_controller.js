angular.module('trycatch').controller("LogInController", ['$scope', '$state', 'Auth', 'TokenGen', function($scope, $state, Auth, TokenGen){
  $scope.user_authenticated = false;
  $scope.is_admin = false;

  $scope.login = function(email, password){
    Auth.login({ email: $scope.email, password: $scope.password });
  };

  $scope.$on("devise:new-session", function(event, user){
    location.href = "/";
  });

  $scope.$on("devise:login", function(event, user){
    TokenGen.setToken(user.auth_token);
    $scope.user_authenticated = true;
    $scope.current_user = user;
  });

}]);
