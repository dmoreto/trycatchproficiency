angular.module('trycatch').factory("TokenGen", function(){
  var data = {};

  data.getToken = function(){
    return data.token;
  };

  data.setToken = function(token){
    data.token = token;
  };

  return data;
});
