angular.module('trycatch').controller("PostsController", ['$scope', '$http', '$stateParams', 'Auth', 'TokenGen', function($scope, $http, $stateParams, Auth, TokenGen){
  $scope.posts = [];
  $scope.someone = "";

  Auth.currentUser().then(function(current_user){
    $scope.current_user = current_user;
    get_posts(current_user.id);
  });

  get_posts = function(current_user_id){
    if(current_user_id == $stateParams.user_id) {
      $scope.someone = "My ";
    }

    $http.get("/api/users/" +$stateParams.user_id+ "/posts", { headers: { "Authorization": TokenGen.getToken() } })
         .success(function(data){
           $scope.posts = data;
         })
         .error(function(){
           alert("An error ocurred while trying to get posts");
         });
  };

  $scope.delete_post = function(post) {
    $http.delete("/api/users/" +$stateParams.user_id+ "/posts/" +post.id, { headers: { "Authorization": TokenGen.getToken() } })
         .success(function(){
           $scope.posts.splice($scope.posts.indexOf(post), 1)
         })
         .error(function(){
           alert("An error ocurred while trying to delete this post");
         });
  };

}]);


angular.module('trycatch').controller("PostAddController", ['$scope', '$state', '$stateParams', 'Auth', 'TokenGen', function($scope, $http, $state, $stateParams, Auth, TokenGen){
  $scope.post = {}

  Auth.currentUser().then(function(current_user){
    TokenGen.setToken(current_user.auth_token);
  });

  $scope.publish_post = function(post) {
    $http.post("/api/users/" +$stateParams.user_id+ "/posts", { post: { content: $scope.post.content, user_id: $stateParams.user_id } },
              { headers: { "Authorization": TokenGen.getToken() } })
         .success(function(){
           alert("Your post was published successfully");
           $state.go("api.posts.list", { user_id: $stateParams.user_id });
         })
         .error(function(){
           alert("An error ocurred while trying to publish your post");
         });
  };
}]);


angular.module('trycatch').controller("PostEditController", ['$scope', '$http', '$state', '$stateParams', 'Auth', 'TokenGen', function($scope, $http, $state, $stateParams, Auth, TokenGen){
  $scope.post = {}

  Auth.currentUser().then(function(current_user){
    TokenGen.setToken(current_user.auth_token);
    get_post();
  });

  get_post = function(){
    $http.get("/api/users/" +$stateParams.user_id+ "/posts/" +$stateParams.post_id,
              { headers: { "Authorization": TokenGen.getToken() } })
         .success(function(data){
           $scope.post.content = data.post.content;
         })
         .error(function(){
           alert("An error ocurred while trying to get your post");
         });
  };

  $scope.edit_post = function(post) {
    $http.patch("/api/users/" +$stateParams.user_id+ "/posts/" +$stateParams.post_id,
               { post: { content: $scope.post.content, user_id: $stateParams.user_id, post_id: $stateParams.post_id } },
               { headers: { "Authorization": TokenGen.getToken() } })
         .success(function(){
           alert("Your post was updated successfully");
           $state.go("api.posts.list", { user_id: $stateParams.user_id });
         })
         .error(function(){
           alert("An error ocurred while trying to update your post");
         });
  };
}]);
