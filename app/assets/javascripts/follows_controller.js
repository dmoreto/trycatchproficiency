angular.module('trycatch').controller("FollowsController", ['$scope', '$http', 'Auth', 'TokenGen', function($scope, $http, Auth, TokenGen){
  $scope.followers = [];
  $scope.followeds = [];

  Auth.currentUser().then(function(current_user){
    get_followers(current_user.id);
    get_followeds(current_user.id);
  });

  get_followers = function(id_user){
    $http.get("/api/users/" +id_user+ "/followers", { headers: { "Authorization": TokenGen.getToken() } })
         .success(function(data){
           $scope.followers = data.followers;
         })
         .error(function(){
           alert("An error ocurred while trying to get followers");
         });
  };

  get_followeds = function(id_user){
    $http.get("/api/users/" +id_user+ "/followings", { headers: { "Authorization": TokenGen.getToken() } })
         .success(function(data){
           $scope.followings = data.followings;
         })
         .error(function(){
           alert("An error ocurred while trying to get followeds");
         });
  };

}]);
