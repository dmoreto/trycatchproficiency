class User < ActiveRecord::Base
  rolify

  devise :database_authenticatable, :rememberable

  # Here, it's needed to invert the logic because "When I have followeds (people I follow) I am a follower"
  has_many :followeds, class_name: "Follow", foreign_key: "follower_id"
  has_many :followers, class_name: "Follow", foreign_key: "followed_id"

  has_many :my_followings, through: :followeds, source: :followed
  has_many :my_followers, through: :followers, source: :follower

  has_many :posts

  validates_uniqueness_of :auth_token

  before_create :generate_authentication_token!


  def generate_authentication_token!
    loop do
      self.auth_token = Devise.friendly_token
      break unless self.class.exists?(auth_token: auth_token)
    end
  end

end
