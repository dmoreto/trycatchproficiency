require 'api_constraints'

Rails.application.routes.draw do
  devise_for :users, controllers: { sessions: 'sessions' }
    
  root "dashboard#index"

  namespace :api, defaults: { format: :json }, path: '/api' do
    scope module: :v1, constraints: ApiConstraints.new(version: 1, default: true) do
      resources :sessions, only: [:create, :destroy]
      resources :users, only: [:index, :create, :show, :update, :destroy] do
        with_options controller: :follows do |c|
          c.post :follow, action: "follow"
          c.delete :unfollow, action: "unfollow"
          c.get :followings, action: "followings"
          c.get :followers, action: "followers"
        end

        resources :posts, only: [:index, :create, :show, :update, :destroy]
      end
    end
  end
end
