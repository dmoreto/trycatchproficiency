require File.expand_path('../boot', __FILE__)

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module TryCatchProficiency
  class Application < Rails::Application
    config.active_record.raise_in_transactional_callbacks = true

    config.generators do |g|
      g.test_framework :rspec
      g.fixture_replacement :factory_girl
      g.template_engine :slim
      g.view_specs false #disable auto generating view specs
      g.integration_specs false #disable auto generating integration specs
      g.helper_specs false #disable auto generating helper specs
      g.helper false #disable auto generating helpers
      g.javascripts false #disable auto generating js files
      g.stylesheets false #disable auto generating css files
    end

    config.angular_templates.module_name    = 'templates'
    config.angular_templates.ignore_prefix  = %w(templates)
    config.angular_templates.inside_paths   = ['app/assets']
    config.angular_templates.markups        = %w(erb slim)
    config.angular_templates.extension      = 'html'
  end
end
